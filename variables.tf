variable "folder_id" {
  type = string
}

variable "ssh_key_user" {
  type    = string
  default = "ubuntu"
}

variable "ssh_key_path" {
  type    = string
  default = "~/.ssh/id_rsa.pub"
}

variable "hostname" {
  type    = string
  default = "compute"
}

variable "name" {
  type    = string
  default = "compute"
}

variable "platform_id" {
  type    = string
  default = "standard-v2"
}

variable "preemptible" {
  type    = bool
  default = true
}

variable "cores" {
  type    = number
  default = 2
}

variable "memory" {
  type    = number
  default = 2
}

variable "core_fraction" {
  type    = number
  default = 20
}

variable "image_id" {
  type = string
}

variable "disk_size" {
  type    = number
  default = 20
}

variable "subnet_id" {
  type = string
}

variable "nat" {
  type    = bool
  default = false
}

variable "labels" {
  type = map(string)
  default = {
    "ansible_group" = "compute"
  }
}
