output "hostname" {
  value = yandex_compute_instance.compute.hostname
}

output "ip_app" {
  value = yandex_compute_instance.compute.network_interface.0.ip_address
}

output "nat_ip" {
  value = yandex_compute_instance.compute.network_interface.0.nat_ip_address
}
