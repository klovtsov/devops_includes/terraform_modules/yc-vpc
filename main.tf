terraform {
  required_providers {
    yandex = {
      source  = "yandex-cloud/yandex"
      version = ">=0.82.0"
    }
  }
}

resource "yandex_compute_instance" "compute" {
  folder_id   = var.folder_id
  hostname    = var.hostname
  name        = var.name
  platform_id = var.platform_id
  resources {
    cores         = var.cores
    memory        = var.memory
    core_fraction = var.core_fraction
  }
  scheduling_policy {
    preemptible = var.preemptible
  }

  boot_disk {
    initialize_params {
      image_id = var.image_id
      size     = var.disk_size
    }
  }

  network_interface {
    subnet_id = var.subnet_id
    nat       = var.nat
  }

  metadata = {
    "ssh-keys" = "${var.ssh_key_user}:${file("${var.ssh_key_path}")}"
  }

  labels = var.labels
}
